>> the variables declared in the JS (ex: myvariable: 'value'; part correspond to the part declared in HTML --> {{myvariable}}
>> Components form the views (which Navigation Bar in app.vue links to)

// Component file:
Template includes the <COMPONENT NAME />

.. other things
Button can $emit.('toggle-add-task'), calling the function
 >> { $emit() function that allows you to pass custom events up the component tree } 
    Example:
     
      <input type="text" v-model="name"> // V-model is two way data binding [similar to variable]
      <button v-on:click="$emit('update', name)">

    Another example:
   <div v-show = "showAddTask"> // conditional display an element
   where you say within Header @toggle-add-task = "toggleAddTask", doing the same thing in App.vue
   [Header Methods now can have "toggleAddTask()"]

Script adds "component: NAME"
 (props: {
     <name>: Object
 })

<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

VUE file layout:
<template>
... // input type, {{ VARIABLE NAME }}, <div's>
    or <span> {{msg}} </span>
    //dynamic arguments: <a v-bind:[attributeName]="url"> ... </a>
</template>

<script>
import // < ANY IMPORTS HERE> -- ex. Task from './Task'

often new Vue instance generated [var vm = new Vue({data: data})]
// Usually have to use Vue.component, but can be locally registered in a module (as presented)

// Equivalent to "MAIN"
export default{
data:function(){ // or just data()
    return{
        title:
        data:
        style:
        layout:
    }
}
// which functions are going to be called?
created:function(){
    this.loadData()
}
//declare the functions
method:function(){
    async loadData() {
        //Variables declared with arrays as brackets (curly within braces)
        var ammo_types = [
                        {description: '7.62mm SDM', name: '7.62mm SDM'},
                        {description: '7.62mm GPMG', name: '7.62mm GPMG'},
                        {description: '5.56mm', name: '5.56mm'},
                        {description: '9mm', name: '9mm'},
                        //{description: '.338', name: '.338'}
                    ]
                    //??? DATA??
                    value: utils.extractColumn(res.data.result, "enemy_complexity")[0],
                    const res = await API.get_metric("complexity", [
                         {col: "mission_type", opr: "eq", value: self.$store.state.filters.mission},
                    ]);

                    self.value = utils.extractColumn(res.data.result,"temp")[0]

        }
    }
    computed{
        // meant for simple operations 
    }
} // END OF EXPORT DEFAULT
</script>
// optional 
<style>{}</style>